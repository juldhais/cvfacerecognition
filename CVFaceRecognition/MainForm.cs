﻿using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CVFaceRecognition
{
    public partial class MainForm : Form
    {
        VideoCapture webcam;
        EigenFaceRecognizer faceRecognizer;
        CascadeClassifier faceDetector;

        Mat mat;

        bool isTraining = false;
        int trainPersonID;
        string trainPersonName;

        List<string> personData;

        const int processedImageWidth = 128;
        const int processedImageHeight = 150;

        int timerCounter = 0;
        int timerLimit = 30;
        int scanCounter = 0;

        Timer timer;

        public MainForm()
        {
            InitializeComponent();

            faceRecognizer = new EigenFaceRecognizer(80, double.PositiveInfinity);

            if (File.Exists("training-data.yml")) faceRecognizer.Read("training-data.yml");

            faceDetector = new CascadeClassifier("haarcascade_frontalface_default.xml");
            
            mat = new Mat();

            personData = File.ReadAllLines("person-data.txt").ToList();

            webcam = new VideoCapture();
            webcam.ImageGrabbed += Webcam_ImageGrabbed;
        }

        private void Webcam_ImageGrabbed(object sender, EventArgs e)
        {
            webcam.Retrieve(mat);
            var image = mat.ToImage<Bgr, byte>();

            if (image != null)
            {
                var grayImage = image.Convert<Gray, byte>();
                var faces = faceDetector.DetectMultiScale(grayImage, 1.3, 5);

                foreach (var face in faces)
                {
                    image.Draw(face, new Bgr(Color.LightGoldenrodYellow), 2);

                    if (!isTraining)
                    {
                        var processedImage = grayImage.Copy(face).Resize(processedImageWidth, processedImageHeight, Emgu.CV.CvEnum.Inter.Cubic);
                        var result = faceRecognizer.Predict(processedImage);

                        var person = personData.FirstOrDefault(x => x.Split(';')[0] == result.Label.ToString());
                        if (!string.IsNullOrWhiteSpace(person))
                        {
                            var personName = person.Split(';')[1];
                            var textPoint = new Point(face.X, face.Y - 10);
                            image.Draw(personName, textPoint, Emgu.CV.CvEnum.FontFace.HersheyDuplex, 1, new Bgr(Color.LightGoldenrodYellow), 2);
                        }
                    }
                }
                
                pictureBox.Image = image.ToBitmap();
            }
        }

        private void buttonTrain_Click(object sender, EventArgs e)
        {
            var form = new AddPersonForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;

                isTraining = true;
                webcam.Start();
                buttonDetect.Enabled = false;
                buttonDetectImage.Enabled = false;
                buttonTrain.Enabled = false;

                trainPersonID = form.PersonID;
                trainPersonName = form.PersonName;

                timer = new Timer();
                timer.Interval = 500;
                timer.Tick += TimerTrain_Tick;
                timer.Start();
            }
        }

        private void TimerTrain_Tick(object sender, EventArgs e)
        {
            if (timerCounter < timerLimit)
            {
                timerCounter++;

                webcam.Retrieve(mat);
                var grayImage = mat.ToImage<Gray, byte>();

                if (grayImage != null)
                {
                    var faces = faceDetector.DetectMultiScale(grayImage, 1.3, 5);
                    if (faces.Count() > 0)
                    {
                        var processedImage = grayImage.Copy(faces[0]).Resize(processedImageWidth, processedImageHeight, Emgu.CV.CvEnum.Inter.Cubic);
                        var filePath = $@"{Directory.GetCurrentDirectory()}\train\{trainPersonID}-{DateTime.Now.Ticks}.jpg";
                        processedImage.Save(filePath);
                        scanCounter++;
                        buttonTrain.Text = $"Training: {scanCounter}";
                    }
                }
            }
            else
            {
                var listFace = new List<Mat>();
                var listID = new List<int>();

                var files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\train");
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file);
                    var splitFileName = fileName.Split('-');
                    var personID = int.Parse(splitFileName[0]);
                    listID.Add(personID);
                        
                    var trainMat = new Mat(file);
                    var trainImage = trainMat.ToImage<Gray, byte>();
                    listFace.Add(trainImage.Mat);
                }

                faceRecognizer.Train(listFace.ToArray(), listID.ToArray());
                faceRecognizer.Write("training-data.yml");

                var newPersonData = $"{trainPersonID};{trainPersonName}";
                personData.Add(newPersonData);
                var stringPersonData = string.Join("\n", personData);
                File.WriteAllText("person-data.txt", stringPersonData);

                timer.Stop();
                timerCounter = 0;
                scanCounter = 0;

                isTraining = false;
                buttonDetect.Enabled = true;
                buttonDetectImage.Enabled = true;
                buttonTrain.Enabled = true;
                buttonTrain.Text = "Train";
                MessageBox.Show("Training complete.");
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonDetect_Click(object sender, EventArgs e)
        {
            webcam.Start();
        }

        private void buttonDetectImage_Click(object sender, EventArgs e)
        {
            webcam.Stop();

            using (var ofd = new OpenFileDialog { Filter = "Image Files|*.jpg;*.png" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    var imageMat = new Mat(ofd.FileName);
                    
                    var newWidth = imageMat.Width > 800 ? 800 : imageMat.Width;
                    var newHeight = imageMat.Height * newWidth / imageMat.Width;
                    var image = imageMat.ToImage<Bgr, byte>().Resize(newWidth, newHeight, Emgu.CV.CvEnum.Inter.Cubic);
                    var grayImage = image.Convert<Gray, byte>();

                    if (grayImage != null)
                    {
                        var personDetected = new List<string>();
                        var faces = faceDetector.DetectMultiScale(grayImage, 1.3, 5);
                        foreach (var face in faces)
                        {
                            image.Draw(face, new Bgr(Color.LightGoldenrodYellow), 2);

                            var processedImage = grayImage.Copy(face).Resize(processedImageWidth, processedImageHeight, Emgu.CV.CvEnum.Inter.Cubic);
                            var result = faceRecognizer.Predict(processedImage);

                            var person = personData.FirstOrDefault(x => x.Split(';')[0] == result.Label.ToString());
                            if (!string.IsNullOrWhiteSpace(person))
                            {
                                var personName = person.Split(';')[1];
                                personDetected.Add(personName);

                                var textPoint = new Point(face.X, face.Y - 10);
                                image.Draw(personName, textPoint, Emgu.CV.CvEnum.FontFace.HersheyDuplex, 1, new Bgr(Color.LightGoldenrodYellow), 2);
                            }
                        }

                        pictureBox.Image = image.ToBitmap();
                    }
                }
            }
        }
    }
}
