﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CVFaceRecognition
{
    public partial class AddPersonForm : Form
    {
        public int PersonID { get; set; }
        public string PersonName { get; set; }

        public AddPersonForm()
        {
            InitializeComponent();
            GetNewPersonID();
        }

        private void GetNewPersonID()
        {
            var persons = File.ReadAllLines("person-data.txt");
            textID.Text = (persons.Count() + 1).ToString();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.PersonID = int.Parse(textID.Text);
            this.PersonName = textName.Text;
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.PersonID = 0;
            this.Name = "";
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
