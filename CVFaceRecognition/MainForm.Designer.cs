﻿namespace CVFaceRecognition
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonTrain = new System.Windows.Forms.Button();
            this.buttonDetect = new System.Windows.Forms.Button();
            this.buttonDetectImage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(12, 48);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(560, 402);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // buttonTrain
            // 
            this.buttonTrain.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTrain.ForeColor = System.Drawing.Color.Black;
            this.buttonTrain.Location = new System.Drawing.Point(472, 12);
            this.buttonTrain.Name = "buttonTrain";
            this.buttonTrain.Size = new System.Drawing.Size(100, 30);
            this.buttonTrain.TabIndex = 3;
            this.buttonTrain.Text = "Train";
            this.buttonTrain.UseVisualStyleBackColor = true;
            this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
            // 
            // buttonDetect
            // 
            this.buttonDetect.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDetect.ForeColor = System.Drawing.Color.Black;
            this.buttonDetect.Location = new System.Drawing.Point(12, 12);
            this.buttonDetect.Name = "buttonDetect";
            this.buttonDetect.Size = new System.Drawing.Size(120, 30);
            this.buttonDetect.TabIndex = 4;
            this.buttonDetect.Text = "Detect Camera";
            this.buttonDetect.UseVisualStyleBackColor = true;
            this.buttonDetect.Click += new System.EventHandler(this.buttonDetect_Click);
            // 
            // buttonDetectImage
            // 
            this.buttonDetectImage.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDetectImage.ForeColor = System.Drawing.Color.Black;
            this.buttonDetectImage.Location = new System.Drawing.Point(138, 12);
            this.buttonDetectImage.Name = "buttonDetectImage";
            this.buttonDetectImage.Size = new System.Drawing.Size(120, 30);
            this.buttonDetectImage.TabIndex = 5;
            this.buttonDetectImage.Text = "Detect Image";
            this.buttonDetectImage.UseVisualStyleBackColor = true;
            this.buttonDetectImage.Click += new System.EventHandler(this.buttonDetectImage_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.buttonDetectImage);
            this.Controls.Add(this.buttonDetect);
            this.Controls.Add(this.buttonTrain);
            this.Controls.Add(this.pictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "JUL | Face Recognition";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button buttonTrain;
        private System.Windows.Forms.Button buttonDetect;
        private System.Windows.Forms.Button buttonDetectImage;
    }
}

